This directory contains source code and coefficients for SD codes, as described 
in the paper "Sector-Disk (SD) Erasure Codes for Mixed Failure Modes in RAID Systems", 
Technical Report UT-CS-13-708, University of Tennessee, May, 2013.  This is a follow-on
to the Usenix 2013 FAST paper, "SD Codes: Erasure Codes Designed for How Storage Systems Really Fail"

The home for this work is https://bitbucket.org/jimplank/sd_codes

