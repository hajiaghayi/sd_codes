/* erase_data.c

   James S. Plank
   plank@cs.utk.edu
   http://web.eecs.utk.edu/~plank

   Professor
   EECS Department
   University of Tennessee
   Knoxville, TN 37996

   January, 2013.

   Part of the SD coding library.  Please see Technical Report UT-CS-13-704
   http://web.eecs.utk.edu/~plank/plank/papers/FAST-2013-SD.html
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <strings.h>

#define talloc(type, num) (type *) malloc(sizeof(type)*(num))

void usage(char *s)
{
  fprintf(stderr, "usage: erase_data n m s r blocksize d_0 .. d_{m-1} s_0 .. s_{m-1}\n");
  if (s != NULL) fprintf(stderr, "%s\n", s);
  exit(1);
}

void print_data(int n, int r, int size, uint8_t **array)
{
  int i, j;

  for (i = 0; i < n*r; i++) {
    for (j = 0; j < size; j++) {
      if (j > 0) printf(" ");
      printf("%02x", array[i][j]);
    }
    printf("\n");
  }
}

main(int argc, char **argv)
{
  int i, j, n, m, r, s, w, c, size;
  int *erased;
  uint8_t **array;

  if (argc < 6) usage(NULL);
  if (sscanf(argv[1], "%d", &n) == 0 || n <= 0) usage("Bad n");
  if (sscanf(argv[2], "%d", &m) == 0 || m <= 0) usage("Bad m");
  if (sscanf(argv[3], "%d", &s) == 0 || s <= 0) usage("Bad s");
  if (sscanf(argv[4], "%d", &r) == 0 || r <= 0) usage("Bad r");
  if (sscanf(argv[5], "%d", &size) == 0 || size <= 0) usage("Bad size");
  if (size % 8 != 0) usage("Size has to be a multiple of 8\n");
  if (argc != 6 + (m+s)) usage("Wrong number of arguments");

  erased = talloc(int, n*r);
  for (i = 0; i < n*r; i++) erased[i] = 0;

  for (i = 0; i < m; i++) {
    if (sscanf(argv[6+i], "%d", &j) != 1 || j < 0 || j >= n) usage("Bad d_x");
    if (erased[j]) usage("Duplicate failed disk");
    while (j < n*r) {
      erased[j] = 1;
      j += n;
    }
  }
  for (i = 0; i < s; i++) {
    if (sscanf(argv[6+m+i], "%d", &j) != 1 || j < 0 || j >= n*r) usage("Bad s_y");
    if (erased[j]) usage("Duplicate failed sector");
    erased[j] = 1;
  }

  array = talloc(uint8_t *, n*r);
  for (i = 0; i < n*r; i++) array[i] = talloc(uint8_t, size);

  for (i = 0; i < n*r; i++) {
    for (j = 0; j < size; j++) {
      if (scanf("%x", &c) != 1) {
        fprintf(stderr, "Bad input at block %d byte %d\n", i, j);
      } else array[i][j] = c; 
    }
  }
  for (i = 0; i < n*r; i++) {
    if (erased[i]) bzero(array[i], size);
  }
  print_data(n, r, size, array);
  exit(0);
}
